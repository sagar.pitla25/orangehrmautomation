﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OrangeHRMAutomation.PageObjects;
using AventStack.ExtentReports;
using AventStack.ExtentReports.Reporter;
using NUnit.Framework.Interfaces;
using System.IO;
using System.Threading;
using OrangeHRMAutomation.Utilities;

namespace OrangeHRMAutomation.Tests
{
    [TestFixture]
    [Parallelizable]
    public class LoginTest : BrowserUtility
    {

        MasterPageObjects page;
        ExtentTest test = null;
        [Test]
        public void AdminLogin()
        {
            try
            {
                page = new MasterPageObjects(driver);
                test = extent.CreateTest("Login").Info("Applicaton logged In");
                page.LoginSite();
                page.VerifyLogin();
                test.Log(Status.Pass, "Login Test Passed");
            }
            catch (Exception e)
            {
                test.Log(Status.Fail, "" + e);
            }  
        }

        [Test]
        public void mousehoverPLA()
        {
            try 
            { 
            page = new MasterPageObjects(driver);
            test = extent.CreateTest("Mouse Hover on PLA").Info("Performing Mouse hover on Main screen");
            page.LoginSite();

            page.VerifyLogin();
            test.Log(Status.Info, "Logged In");
            Thread.Sleep(5000);
            page.mousehoverFun();
            Thread.Sleep(3000);
            test.Log(Status.Pass, "Mouse hover on PLA");
        }
            catch (Exception e)
            {
                test.Log(Status.Fail, "" + e);
            }
}


    }
}

