﻿using AventStack.ExtentReports;
using AventStack.ExtentReports.Reporter;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OrangeHRMAutomation.Tests;
namespace OrangeHRMAutomation.Utilities
{
    [TestFixture]
    [Parallelizable]
    public class BrowserUtility
    {
        public IWebDriver driver = null;

        public ExtentReports extent;
        [OneTimeSetUp]
        public void start()
        {
            extent = new ExtentReports();
            var htmlreport = new ExtentHtmlReporter(@"C:\Users\User\source\repos\NewRepo\OrangeHRMAutomation\OrangeHRMAutomation\ExtentRpoetFiles\BrowserUtility.html");
            extent.AttachReporter(htmlreport);
        }
       [SetUp]
        public void Initialize()
        {
            driver = new ChromeDriver();
            driver.Navigate().GoToUrl("https://orangehrm-demo-6x.orangehrmlive.com/");
            driver.Manage().Window.Maximize();
           
            WebDriverWait webDriverWait = new WebDriverWait(driver, TimeSpan.FromSeconds(40));
            

        }
       
        [TearDown]
        public void close()
        {
            //
            driver.Close();
        }
        [OneTimeTearDown]
        public void FClose()
        {
            extent.Flush();
        }
    }
}

