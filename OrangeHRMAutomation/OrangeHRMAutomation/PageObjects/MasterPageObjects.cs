﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.UI;
using OrangeHRMAutomation.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrangeHRMAutomation.PageObjects
{
    class MasterPageObjects : BrowserUtility
    {
       // IWebDriver driver;
   
        public MasterPageObjects(IWebDriver driver)
        {
            this.driver = driver;
        }

        public void LoginSite()
        {
          driver.FindElement(By.Id("txtUsername")).Clear();
          driver.FindElement(By.Id("txtUsername")).SendKeys("admin");
            Timewait();
        
            driver.FindElement(By.Id("txtPassword")).Clear();
            driver.FindElement(By.Id("txtPassword")).SendKeys("admin123");
            Timewait();
       
            driver.FindElement(By.Id("btnLogin")).Click();
            Timewait();
        }

        public void VerifyLogin()
        {
            String title = "Dashboard";
            string ActualTitle =driver.FindElement(By.ClassName("page-title")).Text;
            Assert.AreEqual(title, ActualTitle);
        }

        public void mousehoverFun()
        {
            String tooltipValue = "Pending Leave Requests";
            Actions act = new Actions(driver);
            IWebElement PLA = driver.FindElement(By.Id("dropDownLeaveTrigger"));
            act.MoveToElement(PLA).Perform();
            String Actualttotip = PLA.GetAttribute("data-tooltip");
            Assert.AreEqual(Actualttotip, tooltipValue);
        }

        public void Timewait()
        {
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(40));
        }
    }
}
